import "./App.css";
import React, { Component } from "react";
import Header from "./Components/Header/Header";
import Products from "./Components/Products/Products";
import AddItem from "./Components/AddItem/AddItem";
import { Routes, Route } from "react-router-dom";
import EditItem from "./Components/EditItem/EditItem";
import axios from "axios";

class App extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    };
  }

  componentDidMount() {
    axios
      .get("https://fakestoreapi.com/products")
      .then((res) => {
        return res.data;
      })
      .then((productData) =>
        this.setState({
          ...this.state,
          products: productData,
          status: this.API_STATES.LOADED,
        })
      )
      .catch((error) => {
        console.error(error.message);
        this.setState({ ...this.state, status: this.API_STATES.ERROR });
      });
  }

  handleProductUpdate = (updatedProduct) => {
    const updatedProducts = this.state.products.map((product) =>
      product.id === updatedProduct.id ? updatedProduct : product
    );
    this.setState({
      ...this.state.products,
      products: updatedProducts,
    });
  };

  handleDelete = (id) => {
    this.setState({
      products: [
        ...this.state.products.filter((item) => {
          return item.id !== id;
        }),
      ],
    });
  };
  handleAdd = (addData) => {
    this.setState({ products: [...this.state.products, addData] });
  };

  render() {
    return (
      <div>
        <Header />
        <Routes>
          <Route
            path="/"
            element={
              <Products
                API_STATES={this.API_STATES}
                productsData={this.state}
                handleDelete={this.handleDelete}
              />
            }
          ></Route>
          <Route
            path="editPage/:productId"
            element={
              <EditItem
                API_STATES={this.API_STATES}
                productsDetails={this.state}
                handleProductUpdate={this.handleProductUpdate}
              />
            }
          ></Route>
          <Route
            path="additem"
            element={<AddItem handleAdd={this.handleAdd} />}
          ></Route>
        </Routes>
      </div>
    );
  }
}

export default App;
