import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";
import "./AddItem.css";

export class AddItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "id": uuidv4(),
      "title": "",
      "description": "",
      "image": "",
      "rating": {
        "rate": 3,
        "count": 1,
      },
      "price": "",
      "category": "",
      "dataFlag": true,
      "itemAdd": false,
    };
  }

  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({
      ...this.state,
      [name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (
      ["jpg", "jpeg", "png", "gif"].includes(
        this.state.image.toLowerCase().split(".").pop()
      )
    ) {
      this.props.handleAdd(this.state);
      this.setState({
        ...this.state,
        itemAdd: true,
      });
      this.setState({
        id: uuidv4(),
        title: "",
        description: "",
        image: "",
        rating: {
          rate: 3,
          count: 1,
        },
        price: "",
        category: "",
      });
    } else {
      {
        this.props.handleAdd(this.state);
        this.setState({
          ...this.state,
          dataFlag: false,
        });
      }
    }
  };

  render() {
    return (
      <div className="add-product-item">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group" on>
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control mt-1"
              name="title"
              placeholder="Enter title"
              value={this.state.title}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="category">Category</label>
            <select
              placeholder="Enter price"
              className="form-control mt-1"
              name="category"
              onChange={this.handleChange}
              required
            >
              <option value="">Select Category</option>
              <option value="men's clothing">men's clothing</option>
              <option value="jewelery">jewelery</option>
              <option value="electronics">electronics</option>
              <option value="women's clothing">women's clothing</option>
            </select>
          </div>
          <div className="form-group mt-3">
            <label htmlFor="category">Description</label>
            <textarea
              className="form-control mt-1"
              type="text"
              placeholder="Enter description"
              name="description"
              onChange={this.handleChange}
              value={this.state.description}
              required
              rows="3"
            ></textarea>
          </div>
          <div className="form-group mt-3">
            <label htmlFor="price">Price</label>
            <input
              type="number"
              className="form-control mt-1"
              name="price"
              placeholder="Enter price"
              value={this.state.price}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="imageurl">Image URL</label>
            <input
              type="text"
              className="form-control mt-1"
              name="image"
              value={this.state.image}
              placeholder="Enter image URL"
              onChange={this.handleChange}
              required
            />
            {this.state.dataFlag ? "" : <p>Enter valid image url</p>}
          </div>
          <button type="submit" className="btn btn-primary mt-3">
            Add Item
          </button>
          {this.state.itemAdd ? <p>Item added succesfully</p> : ""}
        </form>
      </div>
    );
  }
}

export default AddItem;
