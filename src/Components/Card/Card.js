import React, { Component } from "react";
import "./Card.css";
import { Link } from "react-router-dom";

class Card extends Component {
  render() {
    const { product, handleDelete } = this.props;
    return (
      <div className="card" key={product.id}>
        <div className="icon-status">
          <Link to={`editPage/${product.id}`}>
            <span>
              <i className="fas fa-edit"></i>Edit
            </span>
          </Link>
          <span onClick={() => handleDelete(product.id)}>
            <i className="fa-solid fa-trash"></i>delete
          </span>
        </div>
        <img src={product.image} alt="product-image" className="card-image" />

        <div>
          Price<span className="text-bold"> : {product.price}$</span>
        </div>
        <div>{product.title}</div>
        <div>
          Rating<span className="text-bold"> : {product.rating.rate}</span>
        </div>
        <div>
          Category<span className="text-bold"> : {product.category}</span>
        </div>
        <div>
          Count<span className="text-bold"> : {product.rating.count}</span>
        </div>
      </div>
    );
  }
}

export default Card;
