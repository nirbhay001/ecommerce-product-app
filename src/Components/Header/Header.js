import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Header.css";

export class Header extends Component {
  render() {
    return (
      <div className="header">
        <span>
          <Link to="/">
            <i className="fa-solid fa-house cart-icon"></i>
          </Link>
        </span>
        <span className="product-type">
          <Link to="/">
            <span className="product-link">Ecommerce</span>
          </Link>
        </span>
        <div className="add-product">
          <Link to="/addItem">
            <button className="add-button">Add Item</button>
          </Link>
          <span className="cart-icon">
            <i className="fa-solid fa-cart-shopping .bg-dark"></i>
          </span>
        </div>
      </div>
    );
  }
}

export default Header;
