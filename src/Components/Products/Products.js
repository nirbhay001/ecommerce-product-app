import React, { Component } from "react";
import Card from "../Card/Card";
import { InfinitySpin } from "react-loader-spinner";
import "./Products.css";

class Products extends Component {
  render() {
    const { API_STATES, productsData, handleDelete } = this.props;
    return (
      <>
        {productsData.status === API_STATES.LOADING ? (
          <div className="spinner">
            <InfinitySpin width="200" color="#4fa94d" />
          </div>
        ) : productsData.status === API_STATES.ERROR ? (
          <h1 className="data-flag">Error occured in fetching the data </h1>
        ) : productsData.products.length === 0 ? (
          <h1 className="data-flag">Data not Found</h1>
        ) : (
          <div className="products">
            {productsData.products.map((product) => {
              return (
                <Card
                  product={product}
                  key={product.id}
                  handleDelete={handleDelete}
                />
              );
            })}
          </div>
        )}
      </>
    );
  }
}

export default Products;
